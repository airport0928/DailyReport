-- phpMyAdmin SQL Dump
-- version 3.4.10.1
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2014 年 03 月 30 日 05:20
-- 服务器版本: 5.5.20
-- PHP 版本: 5.3.10

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `innovation`
--

-- --------------------------------------------------------

--
-- 表的结构 `tbl_bool`
--

CREATE TABLE IF NOT EXISTS `tbl_bool` (
  `id` tinyint(11) NOT NULL,
  `is_true` varchar(5) COLLATE utf8_unicode_ci DEFAULT 'Yes',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- 表的结构 `tbl_dailyreport`
--

CREATE TABLE IF NOT EXISTS `tbl_dailyreport` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `author_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `author_id` (`author_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3597 ;

-- --------------------------------------------------------

--
-- 表的结构 `tbl_file`
--

CREATE TABLE IF NOT EXISTS `tbl_file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `size` float NOT NULL,
  `type` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `author_id` int(11) DEFAULT NULL,
  `info` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `upload_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `author_id` (`author_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=122 ;

-- --------------------------------------------------------

--
-- 表的结构 `tbl_post`
--

CREATE TABLE IF NOT EXISTS `tbl_post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `author_id` int(11) NOT NULL,
  `post_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `title` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `author_id` (`author_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- 表的结构 `tbl_project`
--

CREATE TABLE IF NOT EXISTS `tbl_project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

-- --------------------------------------------------------

--
-- 表的结构 `tbl_room`
--

CREATE TABLE IF NOT EXISTS `tbl_room` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `roomname` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- 表的结构 `tbl_user`
--

CREATE TABLE IF NOT EXISTS `tbl_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `roomid` int(11) DEFAULT NULL,
  `projectid` int(11) DEFAULT NULL,
  `receive_email` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否接收邮件',
  `receive_remind` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否接收日报提醒',
  `week_count` int(11) DEFAULT '0',
  `all_count` int(11) DEFAULT '0',
  `week_off_count` int(11) DEFAULT '0',
  `off` tinyint(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_r` (`roomid`),
  KEY `fk_p` (`projectid`),
  KEY `receive_email` (`receive_email`),
  KEY `receive_remind` (`receive_remind`),
  KEY `off` (`off`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=36 ;

--
-- 限制导出的表
--

--
-- 限制表 `tbl_dailyreport`
--
ALTER TABLE `tbl_dailyreport`
  ADD CONSTRAINT `tbl_dailyreport_ibfk_1` FOREIGN KEY (`author_id`) REFERENCES `tbl_user` (`id`);

--
-- 限制表 `tbl_file`
--
ALTER TABLE `tbl_file`
  ADD CONSTRAINT `tbl_file_ibfk_1` FOREIGN KEY (`author_id`) REFERENCES `tbl_user` (`id`);

--
-- 限制表 `tbl_post`
--
ALTER TABLE `tbl_post`
  ADD CONSTRAINT `tbl_post_ibfk_1` FOREIGN KEY (`author_id`) REFERENCES `tbl_user` (`id`);

--
-- 限制表 `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD CONSTRAINT `fk_p` FOREIGN KEY (`projectid`) REFERENCES `tbl_project` (`id`),
  ADD CONSTRAINT `fk_r` FOREIGN KEY (`roomid`) REFERENCES `tbl_room` (`id`),
  ADD CONSTRAINT `tbl_user_ibfk_1` FOREIGN KEY (`receive_email`) REFERENCES `tbl_bool` (`id`),
  ADD CONSTRAINT `tbl_user_ibfk_2` FOREIGN KEY (`receive_remind`) REFERENCES `tbl_bool` (`id`),
  ADD CONSTRAINT `tbl_user_ibfk_3` FOREIGN KEY (`receive_remind`) REFERENCES `tbl_bool` (`id`),
  ADD CONSTRAINT `tbl_user_ibfk_4` FOREIGN KEY (`off`) REFERENCES `tbl_bool` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
